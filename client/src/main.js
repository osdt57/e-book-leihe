import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
//import gsap from "gsap"

Vue.config.productionTip = false

Axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;
/*
Vue.mixin({
  created: function () {
    this.gsap = gsap;
  }
});
*/

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
