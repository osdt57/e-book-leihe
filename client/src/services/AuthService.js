import axios from 'axios';
const url = 'http://localhost:3000/api/';

export default {
    login(credentials) {
        return axios
            .post(url + 'login/', credentials)
            .then(response => response.data);
    },
    signUp(credentials) {
        return axios
            .post(url + 'sign-up/', credentials)
            .then(response => response.data);
    },
    getSecretContent() {
        return axios.get(url + 'secret-route/').then(response => response.data);
    },
    getBooks(){
        return axios.get(url+'books/').then(response => response.data);
    },
    getFaecher(){
        return axios.get(url+'faecher/').then(response=>response.data)
    },

    getFaecherBooks(credentials){
        return axios.post(url+'/faecher-books',credentials).then(response=>response.data)
    },
    getBook(credentials){
        return axios.post(url+'/book',credentials).then(response=>response.data)
    },
    getSearch(credentials){
        return axios.post(url+'/search',credentials).then(response=>response.data)
    },
    toLibrary(credentials){
        return axios.post(url+'/toLibrary',credentials).then(response=>response.data)
    },
    inLibrary(credentials){
        return axios.post(url+'/inLibrary',credentials).then(response=>response.data)
    },
    getLibrary(credentials){
        return axios.post(url+'/getLibrary',credentials).then(response=>response.data)
    },
    getLastRead(credentials){
        return axios.post(url+'/getLastRead',credentials).then(response=>response.data)
    },
    setLastRead(credentials){
        return axios.post(url+'/setLastRead',credentials).then(response=>response.data)
    },
    getSchuelerData(credentials){
        return axios.post(url+'/getSchuelerData',credentials).then(response=>response.data)
    },
    getUserDataLehrer(credentials){
        return axios.post(url+'/getUserDataLehrer',credentials).then(response=>response.data)
    },
    isLehrer(credentials){
        return axios.post(url+'/isLehrer',credentials).then(response=>response.data)
    },
    changePassword(credentials){
        return axios.post(url+'/changePassword',credentials).then(response=>response.data)
    },
    addInviteCode(credentials){
        return axios.post(url+'/addInviteCode',credentials).then(response=>response.data)
    },
    getInviteCode(credentials){
        return axios.post(url+'/getCodes',credentials).then(response=>response.data)
    },
    getJahrgaenge(){
        return axios.get(url+'/getJahrgaenge').then(response=>response.data)
    },
    
    
    
};