const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'e-book-leihe',
  password: ''
});
connection.connect();
module.exports = connection;