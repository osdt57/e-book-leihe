const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const db = require('../lib/db.js');
const userMiddleware = require('../middleware/users.js');


router.post('/sign-up', userMiddleware.validateRegister, (req, res, next) => {
    db.query(
        `SELECT * FROM users WHERE Benutzername = LOWER(${db.escape(
            req.body.username
        )});`,
        (err, result) => {
            if (result.length) {
                return res.status(409).send({
                    msg: 'This username is already in use!'
                });
            } else {
                db.query(
                    `select maxVerwendungen from einladungscode where idEinladungscode = LOWER(${db.escape(
                        req.body.einladungscode
                    )});`,
                    (err, result) => {
                        console.log(result[0]['maxVerwendungen'] + ":" + result.length)
                        if (result.length == 0 && result[0]['maxVerwendungen'] > 0) {
                            return res.status(409).send({
                                msg: 'Invalid Invitecode'
                            });
                        } else {
                            // username is available
                            bcrypt.hash(req.body.password, 10, (err, hash) => {
                                if (err) {
                                    return res.status(500).send({
                                        msg: err
                                    });
                                } else {
                                    // has hashed pw => add to database

                                    db.query(
                                        `call createSchueler(?,?,?,?,?)`, [req.body.username, req.body.name, req.body.vorname, hash, req.body.einladungscode],
                                        (err, result) => {
                                            if (err) {
                                                throw err;
                                                return res.status(400).send({
                                                    msg: err
                                                });
                                            }
                                            return res.status(201).send({
                                                msg: 'Registered!'
                                            });
                                        }
                                    );
                                }
                            });

                        }
                    }
                );
            }
        }
    );
});

router.post('/login', (req, res, next) => {
    db.query(
        `SELECT * FROM users WHERE Benutzername = ${db.escape(req.body.username)};`,
        (err, result) => {
            // user does not exists
            if (err) {
                throw err;
                return res.status(400).send({
                    msg: err
                });
            }
            if (!result.length) {
                return res.status(401).send({
                    msg: 'Username or password is incorrect!'
                });
            }
            // check password
            bcrypt.compare(
                req.body.password,
                result[0]['password'],
                (bErr, bResult) => {
                    // wrong password
                    if (bErr) {
                        throw bErr;
                        return res.status(401).send({
                            msg: 'Username or password is incorrect!1'
                        });
                    }
                    if (bResult) {
                        db.query(`Update users set LetzteAnmeldung=now() where UserId=${result[0]['UserId']}`)
                        const token = jwt.sign({
                            username: result[0].username,
                            userId: result[0].id
                        },
                            'SECRETKEY', {
                            expiresIn: '7d'
                        }
                        );
                        return res.status(200).send({
                            msg: 'Logged in!',
                            token,
                            user: result[0]
                        });
                    }
                    return res.status(401).send({
                        msg: 'Username or password is incorrect!2'
                    });
                }
            );
        }
    );
});

router.post('/faecher-books', (req, res, next) => {
    db.query(`SELECT * FROM buecher b join fach f on b.idFach=f.idFach where f.Fachname=${db.escape(req.body.idFach)};`, (err, result) => {
        return res.status(200).send({
            books: result
        });
    })
})

router.post('/book', (req, res, next) => {
    db.query(`SELECT * FROM buecher b join fach f on b.idFach=f.idFach where b.ISBN=${db.escape(req.body.isbn)};`, (err, result) => {

        return res.status(200).send({
            book: result
        });
    })
})

router.get('/secret-route', userMiddleware.isLoggedIn, (req, res, next) => {
    console.log(req.userData);
    res.send('This is the secret content. Only logged in users can see that!');
});

router.get('/books', (req, res, next) => {
    db.query('SELECT * FROM buecher b join fach f on b.idFach=f.idFach ;', (err, result) => {
        return res.status(200).send({
            books: result
        });
    })
});
router.get('/faecher', (req, res, next) => {
    db.query('SELECT * FROM fach;', (err, result) => {
        return res.status(200).send({
            faecher: result
        });
    })
});

router.post('/search', (req, res, next) => {
    db.query(
        `call searchBook(?)`, [req.body.searchStr], (err, result) => {
            return res.status(200).send({
                books: result
            });
        })
});


router.post('/toLibrary', (req, res, next) => {
    if (req.body.add) {
        console.log(req.body.idUser)
        db.query(
            `call addToLibrary(?,?)`, [req.body.isbn, req.body.idUser], (err, result) => {
                return res.status(200).send({
                    msg: "added"
                });
            })
    } else {
        db.query(
            `call removeFromLibrary(?,?)`, [req.body.isbn, req.body.idUser], (err, result) => {
                return res.status(200).send({
                    msg: "removed"
                });
            })
    }
});
router.post('/inLibrary', (req, res, next) => {
    db.query(
        `select * from bibliothek_positionen where idBuchISBN=${db.escape(req.body.isbn)} and idUser=${db.escape(req.body.idUser)}`, (err, result) => {
            if (result.length) {
                return res.status(200).send({
                    inLibrary: true
                });
            } else {
                return res.status(200).send({
                    inLibrary: false
                });
            }
        })

});

router.post('/getLibrary', (req, res, next) => {
    db.query(
        `call getLibrary(?)`, [req.body.idUser], (err, result) => {
            return res.status(200).send({
                books: result
            });
        })
});

router.post('/getLastRead', (req, res, next) => {
    db.query(
        `call getLastRead(?)`, [req.body.idUser], (err, result) => {
            return res.status(200).send({
                books: result
            });
        })
});
router.post('/setLastRead', (req, res, next) => {
    db.query(
        `call lastRead(?,?)`, [req.body.isbn, req.body.idUser], (err, result) => {
            return res.status(200).send({
                msg: "added"
            });
        })
});
router.post('/getSchuelerData', (req, res, next) => {
    db.query(
        `call getUserDataSchueler(?)`, [req.body.idUser], (err, result) => {
            return res.status(200).send({
                user: result[0]
            });
        })
});
router.post('/getUserDataLehrer', (req, res, next) => {
    db.query(
        `call getUserDataLehrer(?)`, [req.body.idUser], (err, result) => {
            return res.status(200).send({
                user: result[0]
            });
        })
});
router.post('/isLehrer', (req, res, next) => {
    db.query(
        `select * from lehrer where idLehrer=${db.escape(req.body.idUser)}`, (err, result) => {
            if (result.length > 0) {
                return res.status(200).send({
                    isLehrer: true
                });
            } else {
                return res.status(200).send({
                    isLehrer: false
                });
            }

        })
});
router.post('/changePassword', (req, res, next) => {
    db.query(
        `SELECT * FROM users WHERE UserId = ${db.escape(req.body.UserId)};`,
        (err, result) => {
            bcrypt.compare(
                req.body.password,
                result[0]['password'],
                (bErr, bResult) => {
                    // wrong password
                    if (bErr) {
                        throw bErr;
                        return res.status(401).send({
                            msg: ' password is incorrect!1'
                        });
                    }
                    if (bResult) {
                        bcrypt.hash(req.body.passwordNew, 10, (err, hash) => {
                            if (err) {
                                return res.status(500).send({
                                    msg: err
                                });
                            } else {
                                db.query(`Update users set password=${db.escape(hash)} where UserId=${req.body.UserId}`)
                                return res.status(200).send({
                                    msg: 'Passwort geändert'
                                });
                            }
                        });
                    } else {
                        return res.status(401).send({
                            msg: 'password is incorrect!2'
                        });
                    }
                }
            );
        }
    );

});

router.post('/addInviteCode', (req, res, next) => {
    const keycode=Math.floor((Math.random() * 9999999999) + 1000000000);
    db.query(
        `call generateInviteKey(?,?,?,?)`, [req.body.idUser,req.body.idJahrgang,req.body.maxVerwendungen,keycode], (err, result) => {
            return res.status(200).send({
                codes: result
            });
        })
});
router.post('/getCodes', (req, res, next) => {
    const keycode=Math.floor((Math.random() * 99999999) + 10000000);
    db.query(
        `select * from einladungscode e where e.idLehrer=${db.escape(req.body.idUser)} order by Datum DESC;`, (err, result) => {
            return res.status(200).send({
                codes: result
            });
        })
});
router.get('/getJahrgaenge', (req, res, next) => {
    db.query('SELECT * FROM jahrgangsstufe;', (err, result) => {
        return res.status(200).send({
            jahrgaenge: result
        });
    })
});

module.exports = router;