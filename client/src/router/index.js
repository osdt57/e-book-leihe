import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Bibliothek',
    name: 'Bibliothek',
    component:() => import('../views/Bibliothek.vue')
  },
  {
    path: '/Fach',
    name: 'Fach',
    component:() => import('../views/Fach.vue')
  },
  {
    path: '/Login',
    name: 'Login',
    component:() => import('../views/Login.vue')
  },
  {
    path: '/Registrieren',
    name: 'Register',
    component:() => import('../views/Register.vue')
  },
  {
    path:'/Fach/:group',
    name: 'Fachgroupe',
    props:true,
    component:()=> import('../views/Fach-Groupe-View.vue')
  },
  {
    path:'/Buch/:isbn',
    name:'ViewBook',
    props:true,
    component:()=>import('../views/Book-View.vue')
  },
  {
    path:'/Suche/:search',
    name:"SearchBooks",
    props:true,
    component:()=>import('../views/Suche-View.vue')
  },
  {
    path:'/Profil',
    name:"Profil",
    component:()=>import('../views/Profil.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
